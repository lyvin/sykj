﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Sykj.Components
{
    /// <summary>
    /// 读取系统配置信息
    /// </summary>
    public static class BaseConfig
    {
        /// <summary>
        /// 设置/获取系统的服务提供器
        /// </summary>
        public static IServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// 获取配置
        /// </summary>
        public static IConfiguration Configuration { get; set; }

        /// <summary>
        /// 获取应用程序路径
        /// </summary>
        public static string ContentRootPath { get; set; }

        /// <summary>
        /// 获取静态资源根路径
        /// </summary>
        public static string WebRootPath { get; set; }

        /// <summary>
        /// 设置基础信息
        /// </summary>
        /// <param name="config"></param>
        /// <param name="contentRootPath"></param>
        /// <param name="webRootPath"></param>
        public static void SetBaseConfig(IConfiguration config, string contentRootPath, string webRootPath)
        {
            Configuration = config;
            ContentRootPath = contentRootPath;
            WebRootPath = webRootPath;
        }

        /// <summary>
        /// 通过key获取value
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetValue(string key)
        {
            Sykj.IServices.IConfigsys configsys = ServiceProvider.GetService<Sykj.IServices.IConfigsys>();
            return configsys.GetValue(key);
        }

        /// <summary>
        /// 获取主域名
        /// </summary>
        public static string DoMain { get { return GetValue(Constant.DOMAIN); } }

        /// <summary>
        /// 获取api域名
        /// </summary>
        public static string ApiDoMain { get { return GetValue(Constant.APIDOMAIN); } }

        /// <summary>
        /// 获取图片域名
        /// </summary>
        public static string ImagesDoMain { get { return GetValue(Constant.IMAGESDOMAIN); } }

        /// <summary>
        /// 短信发送用户名
        /// </summary>
        public static string SMSUserName { get { return GetValue(Constant.SMSUSERNAME); } }

        /// <summary>
        /// 短信发送key
        /// </summary>
        public static string SMSKey { get { return GetValue(Constant.SMSKEY); } }

        /// <summary>
        /// 微信小程序appid
        /// </summary>
        public static string WechatAppID { get { return GetValue(Constant.WECHATAPPID); } }

        /// <summary>
        /// 微信小程序AppSecret
        /// </summary>
        public static string WechatAppSecret { get { return GetValue(Constant.WECHATAPPSECRET); } }

        /// <summary>
        /// 微信app appid
        /// </summary>
        public static string WxAppID { get { return GetValue(Constant.WXAPPID); } }

        /// <summary>
        /// 微信app AppSecret
        /// </summary>
        public static string WxAppSecret { get { return GetValue(Constant.WXAPPSECRET); } }

        /// <summary>
        /// 微信apikey用于签名（apppay支付）
        /// </summary>
        public static string WxApiKey { get { return GetValue(Constant.WXAPIKEY); } }

        /// <summary>
        /// 微信apikey用于签名（jspay支付）
        /// </summary>
        public static string WxJsApiKey { get { return GetValue(Constant.WXJSAPIKEY); } }

        /// <summary>
        /// 微信apikey用于签名（Nativepay支付）
        /// </summary>
        public static string WxNativeApiKey { get { return GetValue(Constant.WXNATIVEAPIKEY); } }

        /// <summary>
        /// app分享标题
        /// </summary>
        public static string ShareTitle { get { return GetValue(Constant.SHARETITLE); } }

        /// <summary>
        /// app分享内容
        /// </summary>
        public static string ShareContent { get { return GetValue(Constant.SHARECONTENT); } }

        /// <summary>
        /// app分享链接
        /// </summary>
        public static string ShareLinkUrl { get { return GetValue(Constant.SHARELINKURL); } }

        /// <summary>
        /// app分享图片
        /// </summary>
        public static string ShareImgUrl { get { return GetValue(Constant.SHAREIMGURL); } }

        /// <summary>
        /// 个推appid
        /// </summary>
        public static string GeTuiAppId { get { return GetValue(Constant.GETUIAPPID); } }

        /// <summary>
        /// 个推appkey
        /// </summary>
        public static string GeTuiAppKey { get { return GetValue(Constant.GETUIAPPKEY); } }

        /// <summary>
        /// 个推mastersecret
        /// </summary>
        public static string GeTuiMasterSecret { get { return GetValue(Constant.GETUIMASTERSECRET); } }

    }
}
