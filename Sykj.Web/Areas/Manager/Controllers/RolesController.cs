﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.Infrastructure;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class RolesController : MController
    {
        IRoles _roles;
        IPermissions _permissions;
        ICacheService _cacheService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="permissions"></param>
        /// <param name="cacheService"></param>
        public RolesController(IRoles roles, IPermissions permissions, ICacheService cacheService)
        {
            _roles = roles;
            _permissions = permissions;
            _cacheService = cacheService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        public IActionResult List(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND Title.Contains(@0) ");
            }

            var list = _roles.GetPagedList(page, limit, strWhere.ToString(), " roleID desc ", keyWords);
            int recordCount = _roles.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Authen()
        {
            return View();
        }

        ///
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult RoleTree()
        {
            var list = _roles.GetTreeList();
            return Json(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult PermissionsTree()
        {
            var list = _permissions.GetTreeList();
            return Json(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public IActionResult RolePermissions(int roleId)
        {
            var list = _roles.GetPermissionsList(roleId);
            return Json(list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="datas"></param>
        /// <returns></returns>
        public IActionResult AddPermissions(List<Sykj.Entity.Rolepermissions> datas)
        {
            _cacheService.RemoveCache(CacheKey.USERPERMISSIONSLIST);
            bool b = _roles.AddPermissions(datas);
            if(datas.Any())
                Components.LogOperate.Add("添加角色权限", "添加ID为【" + datas.First().RoleId + "】的角色的权限", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public IActionResult DeletePermissions(int roleId)
        {
            bool b = _roles.DeletePermissions(roleId);
            Components.LogOperate.Add("删除角色权限", "删除ID为【" + roleId + "】的角色的权限", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// Adds the save.
        /// </summary>
        /// <returns>The save.</returns>
        [HttpPost]
        public IActionResult Add(Sykj.Entity.Roles roles)
        {
            if (!ModelState.IsValid)
            {
                return Json(Failed("数据填写不完整"));
            }
            bool b = _roles.Add(roles);
            Components.LogOperate.Add("添加角色", "添加ID为【" + roles.RoleId + "】的角色", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Edit(int id)
        {
            var model = _roles.GetModel(c => c.RoleId == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Edit(Entity.Roles roles)
        {
            var model = _roles.GetModel(c => c.RoleId == roles.RoleId);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.Title = roles.Title;
            model.Description = roles.Description;
            bool b = _roles.Update(model);
            Components.LogOperate.Add("编辑角色", "编辑ID为【" + roles.RoleId + "】的角色", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            bool b = _roles.DeleteAll(ids);
            Components.LogOperate.Add("批量删除角色", "删除ID为【" + string.Join(",", ids) + "】的角色", HttpContext);
            return Json(AjaxResult(b));
        }
    }
}