/*
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：lh                                           
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-03-08 17:30:16       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Web.Areas.Manager.Controllers                         
*│　类    名： SitemessageController                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 消息管理
    /// </summary>
    public class SiteMessageController : MController
    {
        ISiteMessage _siteMessage;
        IUsers _users;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="siteMessage"></param>
        /// <param name="users"></param>
        public SiteMessageController(ISiteMessage siteMessage,IUsers users)
        {
            _siteMessage = siteMessage;
            _users = users;
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键字</param>
        /// <returns></returns>
        public IActionResult List(int page, int limit,string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND Title.Contains(@0)) ");
            }

            var list = _siteMessage.GetPagedList(page, limit, strWhere.ToString(), " Id desc ", keyWords).ToList();
            list.ForEach(c=>c.ReceivUser=(_users.GetModel(x=>x.UserId==c.ReceiverID)==null?"": _users.GetModel(x => x.UserId == c.ReceiverID).UserName));
            list.ForEach(c => c.ReceivUserTrueName = (_users.GetModel(x => x.UserId == c.ReceiverID) == null ? "" : _users.GetModel(x => x.UserId == c.ReceiverID).TrueName));
            int recordCount = _siteMessage.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
		[ModelValidation]//服务器表单验证
        public IActionResult Add(Sykj.Entity.SiteMessage model)
        {
            var user = _users.GetModel(c => c.UserId == UserId);
            if (user == null)
            {
                return Content("用户信息错误！");
            }
            model.SenderID = user.UserId;
            model.ReceiverIsRead = false;
            model.SenderIsDel = false;
            model.ReaderIsDel = false;
            model.SendTime = DateTime.Now;
            string errorMsg = string.Empty;
            if (_siteMessage.Add(model,ref errorMsg))
            {
                Components.LogOperate.Add("添加消息", "添加标题为【" + model.Title + "】的消息", HttpContext);
                return Json(AjaxResult(true));
            }
            else
            {
                return Json(Failed(errorMsg));
            }
        }

        /// <summary>
        /// 详情
        /// </summary>
        /// <returns></returns>
        public IActionResult Info(int id)
        {
            var model = _siteMessage.GetModel(c => c.Id == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            var user1 = _users.GetModel(c => c.UserId == model.SenderID);
            if (user1 != null)
            {
                model.SenderUser = user1.UserName;
            }
            var user2 = _users.GetModel(c => c.UserId == model.ReceiverID);
            if (user2 != null)
            {
                model.ReceivUser = user2.UserName;
                model.ReceivUserTrueName = user2.TrueName;
            }
            return View(model);
        }
        
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _siteMessage.GetList(c => ids.Contains(c.Id));
            bool b = _siteMessage.Delete(list);
            Components.LogOperate.Add("批量删除消息", "删除ID为【" + string.Join(",", ids) + "】的消息", HttpContext);
            return Json(AjaxResult(b));
        }
    }
}