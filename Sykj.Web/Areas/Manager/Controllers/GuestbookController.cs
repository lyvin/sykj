/*
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：                                                    
*│　作    者：bjg                                            
*│　版    本：1.0    模板代码自动生成                                                
*│　创建时间：2019-02-20 11:49:48       
*│  公    司：云南尚雅科技文化有限公司
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Sykj.Web.Areas.Manager.Controllers                         
*│　类    名： GuestbookController                                 
*└──────────────────────────────────────────────────────────────┘
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 留言
    /// </summary>
    public class GuestbookController : MController
    {
        IGuestbook _guestbook;
        IUsers _users;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="guestbook">留言表</param>
        /// <param name="users">用户表</param>
        public GuestbookController(IGuestbook guestbook, IUsers users)
        {
            _guestbook = guestbook;
            _users = users;
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键字</param>
        /// <returns></returns>
        public IActionResult List(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (NickName.Contains(@0) OR Description.Contains(@0) )");
            }

            var list = _guestbook.GetPagedList(page, limit, strWhere.ToString(), " Id desc ", keyWords);

            int recordCount = _guestbook.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit(int id)
        {
            var model = _guestbook.GetModel(c => c.Id == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            return View(model);
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]//服务器表单验证
        public IActionResult Edit(Sykj.Entity.Guestbook guestbook)
        {
            var model = _guestbook.GetModel(c => c.Id == guestbook.Id);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.ReplyDescription = guestbook.ReplyDescription;
            model.Status = 1;
            model.HandlerDate = DateTime.Now;
            model.HandlerUserId = UserId;
            model.HandlerNickName = _users.GetModel(x => x.UserId == UserId).NickName??"管理员";
            bool b = _guestbook.Update(model);
            Components.LogOperate.Add("回复留言", "回复ID为【" + guestbook.Id + "】的留言", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _guestbook.GetList(c => ids.Contains(c.Id));
            bool b = _guestbook.Delete(list);
            Components.LogOperate.Add("批量删除留言", "删除ID为【" + string.Join(",", ids) + "】的留言", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 审核通过
        /// </summary>
        /// <returns></returns>
        public IActionResult GuestBookAudit(List<int> ids)
        {
            var list = _guestbook.GetList(c => ids.Contains(c.Id)).ToList();
            list.ForEach(c => c.AuditStatus = 1);
            bool b = _guestbook.UpdateRange(list);
            Components.LogOperate.Add("批量审核留言", "审核ID为【" + string.Join(",", ids) + "】的留言", HttpContext);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IActionResult GuestBookNoAudit(List<int> ids)
        {
            var list = _guestbook.GetList(c => ids.Contains(c.Id)).ToList();
            list.ForEach(c => c.Status = 0);
            bool b = _guestbook.UpdateRange(list);
            Components.LogOperate.Add("批量设置未处理留言", "设置ID为【" + string.Join(",", ids) + "】的留言为未处理留言", HttpContext);
            return Json(AjaxResult(b));
        }
    }
}