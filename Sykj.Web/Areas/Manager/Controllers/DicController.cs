using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sykj.Components;
using Sykj.IServices;

namespace Sykj.Web.Areas.Manager.Controllers
{
    /// <summary>
    /// 字典表
    /// </summary>
    public class DicController : MController
    {
        IDic _dic;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="dic"></param>
        public DicController(IDic dic)
        {
            _dic = dic;
        }

        /// <summary>
        /// 列表页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 查询数据
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键字</param>
        /// <returns></returns>
        public IActionResult List(int page, int limit, string keyWords)
        {
            StringBuilder strWhere = new StringBuilder(" 1=1 ");
            if (!string.IsNullOrWhiteSpace(keyWords))
            {
                strWhere.Append(" AND (Name.Contains(@0)) ");
            }

            var list = _dic.GetPagedList(page, limit, strWhere.ToString(), " Id desc ", keyWords);
            int recordCount = _dic.GetRecordCount(strWhere.ToString(), keyWords);
            return Json(ListResult(list, recordCount));
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            ViewBag.ParentIdAll = BindDropList();
            return View();
        }

        /// <summary>
        /// 添加保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]//服务器表单验证
        public IActionResult Add(Sykj.Entity.Dic model)
        {
            var model1 = _dic.GetModel(c => c.Name == model.Name);
            if (model1 != null)
            {
                return Content("键已存在，请更换后重试！");
            }
            model.CreateDate = DateTime.Now;
            model.UserId = UserId;
            bool b = _dic.Add(model);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 编辑
        /// </summary>
        /// <returns></returns>
        public IActionResult Edit(int id)
        {
            var model = _dic.GetModel(c => c.Id == id);
            if (model == null)
            {
                return Content("查询不到数据！");
            }
            ViewBag.ParentIdAll = BindDropList();
            return View(model);
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ModelValidation]//服务器表单验证
        public IActionResult Edit(Sykj.Entity.Dic dic)
        {
            var model = _dic.GetModel(c => c.Id == dic.Id);
            if (model == null)
            {
                return Json(Failed("查询不到该数据"));
            }
            model.Name = dic.Name;
            model.ParentId = dic.ParentId;
            model.Value = dic.Value;
            model.Types = dic.Types;
            model.Remark = dic.Remark;
            bool b = _dic.Update(model);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult DeleteAll(List<int> ids)
        {
            var list = _dic.GetList(c => ids.Contains(c.Id));
            bool b = _dic.Delete(list);
            return Json(AjaxResult(b));
        }

        /// <summary>
        /// 绑定下拉列表信息
        /// </summary>
        public List<SelectListItem> BindDropList()
        {
            List<SelectListItem> itemList = new List<SelectListItem>();
            itemList.Add(new SelectListItem { Text = "请选择父级", Value = "0" });

            var listAll = _dic.GetList().ToList();
            if (listAll.Count > 0)
            {
                var listOne = listAll.Where(c => c.ParentId == 0);
                SelectListItem selectItem;
                foreach (var item in listOne)
                {
                    string text = item.Value;
                    text = "╋" + text;
                    selectItem = new SelectListItem
                    {
                        Text = text,
                        Value = item.Id.ToString()
                    };
                    string blank = "├";
                    itemList.Add(selectItem);

                    BiudTree(item.Id, itemList, listAll, blank);
                }
            }
            return itemList;
        }

        /// <summary>
        /// 递归查询，绑定select
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="itemList"></param>
        /// <param name="dataList"></param>
        /// <param name="blank"></param>
        private void BiudTree(int pId, List<SelectListItem> itemList, List<Entity.Dic> dataList, string blank)
        {
            var list = dataList.Where(c => c.ParentId == pId);
            SelectListItem selectItem;
            foreach (var item in list)
            {
                string text = item.Value;
                text = blank + "『" + text + "』";

                selectItem = new SelectListItem
                {
                    Text = text,
                    Value = item.Id.ToString()
                };

                itemList.Add(selectItem);
                string blank2 = blank + "─";
                BiudTree(item.Id, itemList, dataList, blank2);
            }
        }
    }
}