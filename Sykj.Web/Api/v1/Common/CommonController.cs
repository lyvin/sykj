﻿
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Sykj.Components;
using Sykj.IServices;
using Sykj.ViewModel;

namespace Sykj.Web.Api.v1
{
    /// <summary>
    /// 公共属性
    /// </summary>
    public class CommonController : ApiController
    {
        #region 全局变量
        private readonly IRegions _newRegions;
        #endregion

        #region 构造方法
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="newRegions">省份</param>
        public CommonController(IRegions newRegions)
        {
            _newRegions = newRegions;
        }
        #endregion

        #region 省份列表 BJG
        /// <summary>
        /// 获得省 bjg
        /// </summary>
        /// <param name="parentId">获得省份0 </param>
        /// <returns></returns>
        [HttpGet("newregionlist")]
        public ApiResult NewRegionList(int parentId)
        {
            var list = _newRegions.GetListByCache().Where(x => x.ParentId == "0");
            return DataResult(list);
        }
        #endregion

        #region 关于我们  BJG
        /// <summary>
        /// 账户设置(关于我们，联系我们 )  bjg
        /// </summary>
        /// <param name="keyValue">编码key</param>
        /// <returns></returns>
        [HttpGet("getsystemparameter")]
        public ApiResult GetSystemParameter(string keyValue)
        {
            string value = BaseConfig.GetValue(keyValue);
            return DataResult(value);
        }
        #endregion
    }
}