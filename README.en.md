# sykj

#### Description
自微软的.net core 发布以后，一直想做一套基于.NET平台完整的跨平台部署的系统，于是并有了该系统的诞生，该项目采用经典DDD架构思想进行开发，简洁而不简单，实用至上，所写每一行代码都经过深思熟虑，内置了很多常用组件并且通过Linux系统线上应用的实测，ORM使用微软官方EF支持MySQL、SqlServer、PostgreSQL；后期我们将会不断更新，慢慢接入支付宝，微信支付，标准电商系统等模块。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)