﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
	 	/// <summary>
        /// 省市区
        /// </summary>
	public class Regions
	{
      	/// <summary>
		/// 表主键
        /// </summary>		
        public int AreaId{ get; set; }     
		/// <summary>
		/// 地区ID
        /// </summary>		
        public string RegionId{ get; set; }     
		/// <summary>
		/// 父ID
        /// </summary>		
        public string ParentId{ get; set; }     
		/// <summary>
		/// 地区名称
        /// </summary>		
        public string RegionName{ get; set; }     
		/// <summary>
		/// 拼音
        /// </summary>		
        public string Spell{ get; set; }        
		/// <summary>
		/// 排序
        /// </summary>		
        public int Sort { get; set; }     
		/// <summary>
		/// 路径
        /// </summary>		
        public string Path{ get; set; }     
		/// <summary>
		/// 深度
        /// </summary>		
        public int Depth{ get; set; }     
		   
	}
}

