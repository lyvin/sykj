﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 用户类型
    /// </summary>
    public class Usertype
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
    }
}
