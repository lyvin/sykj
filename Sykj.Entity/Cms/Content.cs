﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 内容实体
    /// </summary>
    public class Content
    {
        /// <summary>
        /// 内容ID
        /// </summary>
        public int ContentId { get; set; }
        /// <summary>
        /// 文章标题
        /// </summary>
        [Display(Name = "文章标题")]
        [Required(ErrorMessage = "此项不能为空")]
        public string Title { get; set; }
        /// <summary>
        /// 文章附标题
        /// </summary>
        [Display(Name = "文章附标题")]
        public string SubTitle { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [Display(Name = "文章简介")]
        public string Summary { get; set; }
        /// <summary>
        /// 文章内容
        /// </summary>
        [Display(Name = "文章内容")]
        //[Required(ErrorMessage = "此项不能为空")]
        public string Description { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        [Display(Name = "文章图片")]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 缩略图
        /// </summary>
        [Display(Name = "缩略图")]
        public string ThumbImageUrl { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public int CreateUserId { get; set; }
        /// <summary>
        /// 最后编辑者
        /// </summary>
        public int? LastEditUserId { get; set; }
        /// <summary>
        /// 最后编辑者
        /// </summary>
        public DateTime? LastEditDate { get; set; }
        /// <summary>
        /// 外部链接
        /// </summary>
        [Display(Name = "外部链接")]
        public string LinkUrl { get; set; }
        /// <summary>
        /// 浏览量
        /// </summary>
        public int PvCount { get; set; }
        /// <summary>
        /// 审核状态 0发布 1待审核 2草稿
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 栏目编号
        /// </summary>
        [Display(Name = "栏目")]
        public int ChannelId { get; set; }
        /// <summary>
        /// 关键字
        /// </summary>
        [Display(Name = "关键字")]
        public string Keywords { get; set; }
        /// <summary>
        /// 顺序
        /// </summary>
        [Display(Name = "顺序")]
        public int Sort { get; set; }
        /// <summary>
        /// 是否推荐
        /// </summary>
        public bool IsRecomend { get; set; }
        /// <summary>
        /// 是否热点
        /// </summary>
        public bool IsHot { get; set; }
        /// <summary>
        /// 是否醒目
        /// </summary>
        public bool IsColor { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        public bool IsTop { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Display(Name = "备注")]
        public string Remary { get; set; }
        /// <summary>
        /// 评论数
        /// </summary>
        public int TotalComment { get; set; }
        /// <summary>
        /// 赞数
        /// </summary>
        public int TotalSupport { get; set; }
        /// <summary>
        /// 收藏数
        /// </summary>
        public int TotalFav { get; set; }
        /// <summary>
        /// 分享数
        /// </summary>
        public int TotalShare { get; set; }
        /// <summary>
        /// 文章来源
        /// </summary>
        [Display(Name = "文章来源")]
        //[Required(ErrorMessage = "此项不能为空")]
        public string BeFrom { get; set; }
        /// <summary>
        /// 附件
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string MetaTitle { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string MetaDescription { get; set; }
        /// <summary>
        /// Keywords
        /// </summary>
        public string MetaKeywords { get; set; }
        /// <summary>
        /// 静态地址
        /// </summary>
        public string StaticUrl { get; set; }
             /// <summary>
             /// 内容类型:0文章 1视频
             /// </summary>
        public int Type { get; set; }

        #region 扩展属性
        /// <summary>
        /// 栏目名称
        /// </summary>
        public string ChannelName;
        #endregion
    }
}
