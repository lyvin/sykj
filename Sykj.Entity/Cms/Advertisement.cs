﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sykj.Entity
{
    /// <summary>
    /// 广告
    /// </summary>
    public class Advertisement
    {
        /// <summary>
        /// 广告编号
        /// </summary>
        public int AdvertisementId { get; set; }
        /// <summary>
        /// 广告名称
        /// </summary>
        [Display(Name = "广告名称")]
        [Required(ErrorMessage = "此项不能为空")]
        public string AdvertisementName { get; set; }
        /// <summary>
        /// 广告位(外键)
        /// </summary>
        [Display(Name = "广告位")]
        [Required(ErrorMessage = "此项不能为空")]
        public int AdvPositionId { get; set; }
        /// <summary>
        /// 展现方式：0:文字 1:图片  2flash   3代码
        /// </summary>
        [Display(Name = "展现方式")]
        [Required(ErrorMessage = "此项不能为空")]
        public int ContentType { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        [Display(Name = "图片地址")]
        public string FileUrl { get; set; }
        /// <summary>
        /// 广告语(文字信息，图片类型的时候，是图片alt信息，文字类型的时候，就是广告的文字)
        /// </summary>
        [Display(Name = "广告语")]
        public string AlternateText { get; set; }
        /// <summary>
        /// 链接:图片和文字的链接
        /// </summary>
        [Display(Name = "链接地址")]
        public string NavigateUrl { get; set; }
        /// <summary>
        /// 自定义代码
        /// </summary>
        [Display(Name = "广告HTML代码")]
        public string AdvHtml { get; set; }
        /// <summary>
        /// 显示频率(广告在当前广告位中的显示频率，数值越大显示的频率越高)
        /// </summary>
        [Display(Name = "显示频率")]
        public int? Impressions { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public int CreateUserId { get; set; }
        /// <summary>
        /// 状态: 1 有效，0 无效 ，-1 欠费停止
        /// </summary>
        [Display(Name = "是否有效")]
        public int Status { get; set; }
        /// <summary>
        /// 起始时间(默认空，无限制)
        /// </summary>
        [Display(Name = "起始时间")]
        public DateTime? StartDate { get; set; }
        /// <summary>
        /// 结束时间(默认空，无限制)
        /// </summary>
        [Display(Name = "结束时间")]
        public DateTime? EndDate { get; set; }
        /// <summary>
        /// 最大PV(默认0 无限制)
        /// </summary>
        [Display(Name = "最大浏览量")]
        public int? DayMaxPv { get; set; }
        /// <summary>
        /// 最大IP(默认0   无限制)
        /// </summary>
        [Display(Name = "最大IP访问")] 
        public int? DayMaxIp { get; set; }
        /// <summary>
        /// 每千次展示价格，默认0
        /// </summary>
        public double? CpmPrice { get; set; }
        /// <summary>
        /// 到最大数自动停止（费用到期自动停止）:1 , 0 , -1(无费用计算)
        /// </summary>
        [Display(Name = "到期自动停止 ")]
        public int? AutoStop { get; set; }
        /// <summary>
        /// 顺序
        /// </summary>
        [Display(Name = "顺序")]
        public int? Sort { get; set; }
        /// <summary>
        /// 业务类型（url：外链接，product：商品详情页，category：商品分类页，suppliershop：商家,articledetails:文章详情）
        /// </summary>
        [Display(Name = "业务类型")]
        public string OperationType { get; set; }
        /// <summary>
        /// 业务标识值
        /// </summary>
        [Display(Name = "业务标识值")]
        public int? TargetId { get; set; }
    }
}
