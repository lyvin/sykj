﻿using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Web.Areas.Manager.Controllers;
using Microsoft.AspNetCore.Mvc;
using Sykj.IServices;
using System.Collections.Generic;
using Sykj.Components;
using Sykj.Infrastructure;

namespace Sykj.Test.Manager
{
    /// <summary>
    /// 高校信息
    /// </summary>
    public class Config : IDisposable
    {
        IServiceProvider _serviceProvider;
        IConfigsys _configsys;
        ICacheService _cacheService;
        ConfigController _configController;

        /// <summary>
        /// 构造方法
        /// </summary>
        public Config()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _configsys = _serviceProvider.GetService<IConfigsys>();
            _cacheService = _serviceProvider.GetService<ICacheService>();
            _configController = new ConfigController(_configsys, _cacheService);
        }

        /// <summary>
        /// 高校列表
        /// </summary>
        /// <param name="page">页</param>
        /// <param name="limit">页大小</param>
        /// <param name="keyWords">关键词</param>
        /// <param name="subScopeName">教育范围</param>
        /// <param name="eduAttrCode">教育属性</param>
        /// <param name="comDepatCode">隶属部门</param>
        /// <param name="schTypeCode">办学类型</param>
        /// <param name="eduLevelCode">学历层次</param>
        ///  <param name="provinceId">省份Id</param>
        ///  <param name="isRecommend">推荐状态</param>
        /// <returns></returns>
        [Theory]
        [InlineData(1,10,"")]
        public void List(int page, int limit, string keyWords)
        {
            JsonResult r = _configController.List(page, limit, keyWords) as JsonResult;
            ApiResult result = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());
            Assert.True(result.success);
        }

        /// <summary>
        /// 增改删测试用例
        /// </summary>
        /// <returns></returns>
        [Fact]
        public void AddEditDelete()
        {
            //增
            Sykj.Entity.Configsys model = new Entity.Configsys()
            {
                KeyName = "测试key",
                KeyValue = "测试value",
                Description = "测试描述"
            };
            _configController.UserId = 88;
            JsonResult r = _configController.Add(model) as JsonResult;
            ApiResult result1 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //改
            model.KeyValue = "测试value123123";
            model.Description = "测试描述233";
            r = _configController.Edit(model) as JsonResult;
            ApiResult result2 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            //删
            List<int> list = new List<int>();
            list.Add(model.CsId);
            r = _configController.DeleteAll(list) as JsonResult;
            ApiResult result3 = Newtonsoft.Json.JsonConvert.DeserializeObject<ApiResult>(r.Value.ToString());

            Assert.True(result1.success&& result2.success&& result3.success);
        }

        public void Dispose()
        {
        }
    }
}
