﻿using Sykj.Infrastructure;
using Sykj.IServices;
using Sykj.Web.Api.v1;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Sykj.ViewModel;
using Sykj.Components;
using static Sykj.ViewModel.EnumHelper;

namespace Sykj.Test.Api.v1
{
    /// <summary>
    /// 会员信息 bjg
    /// </summary>
    public class User : IDisposable
    {
        IServiceProvider _serviceProvider;
        IUsers _users;
        IUserMember _userMember;
        IGuestbook _guestbook;
        IFavorite _favorite;
        UserController _userController;
        IRegions _regions;
        IPointsDetail _pointsDetail;
        ICacheService _cacheService;
        IUserinvite _userInvite;
        /// <summary>
        /// 构造方法
        /// </summary>
        public User()
        {
            _serviceProvider = Factory.BuildServiceProvider();
            _users = _serviceProvider.GetService<IUsers>();
            _userMember = _serviceProvider.GetService<IUserMember>();
            _guestbook = _serviceProvider.GetService<IGuestbook>();
            _favorite = _serviceProvider.GetService<IFavorite>();
            _regions = _serviceProvider.GetService<IRegions>();
            _pointsDetail = _serviceProvider.GetService<IPointsDetail>();
            _cacheService = _serviceProvider.GetService<ICacheService>();
            _userInvite = _serviceProvider.GetService<IUserinvite>();
            _userController = new UserController(_users, _userMember, _guestbook, _favorite, _regions, _pointsDetail, _cacheService, _userInvite);//实例化
        }

        /// <summary>
        /// 学生信息 bjg
        /// </summary>
        [Fact]
        public void MyUserInfo()
        {
            _userController.UserId = 314;
            ApiResult result = _userController.MyUserInfo();
            Assert.True(result.success);
        }

        /// <summary>
        /// 更改用户头像 bjg
        /// </summary>
        [Fact]
        public void UpdateGravatar()
        {
            _userController.UserId = 82;
            ApiResult result = _userController.UpdateGravatar("loadup/xxxx.jpg");
            Assert.True(result.success);
        }

        /// <summary>
        /// 完善用户信息 bjg
        /// </summary>
        [Fact]
        public void UpdateUserInfo()
        {
            _userController.UserId = 82;
            ApiResult result = _userController.UpdateUserInfo("班金国", "男", "2019", "理工", "18213971239");
            Assert.True(result.success);
        }

        /// <summary>
        /// 重置密码 bjg
        /// </summary>
        [Fact]
        public void ModifyPassword()
        {
            _userController.UserId = 82;
            ApiResult result = _userController.ModifyPassword("123456", "123456", "123456");
            Assert.True(result.success);
        }

        /// <summary>
        /// 测试收藏列表 bjg
        /// </summary>
        /// <param name="pageIndex">页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="type">类型</param>
        [Theory]
        [InlineData(1, 5, 4)]
        [InlineData(1, 5, 5)]
        [InlineData(1, 5, 6)]
        public void FavoriteList(int pageIndex, int pageSize, int type)
        {
            _userController.UserId = 82;
            ApiResult result = _userController.FavoriteList(pageIndex, pageSize, type);
            Assert.True(result.success);
        }

        /// <summary>
        /// 添加收藏
        /// </summary>
        [Theory]
        [InlineData(1, 4)]
        [InlineData(3, 4)]
        [InlineData(5, 4)]
        public void addfav(int id, int type)
        {
            _userController.UserId = 82;
            ApiResult result = _userController.AddFav(id, type);
            Assert.True(result.success);
        }

        /// <summary>
        /// 我的留言列表 list
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">页大小</param>
        [Theory]
        [InlineData(1, 5)]
        [InlineData(2, 5)]
        [InlineData(3, 5)]
        public void MyGuestBookList(int pageIndex, int pageSize)
        {
            _userController.UserId = 82;
            ApiResult result = _userController.MyGuestBookList(pageIndex, pageSize);
        }

        /// <summary>
        /// 积分明细
        /// </summary>
        /// <param name="pageIndex">起始页</param>
        /// <param name="pageSize">页大小</param>
        [Theory]
        [InlineData(1, 5)]
        public void MyPoints(int pageIndex, int pageSize)
        {
            ApiResult result = _userController.MyPoints(pageIndex, pageSize);
            Assert.True(result.success);
        }

        /// <summary>
        /// 清除测试数据 
        /// </summary>
        public void Dispose()
        {
            //清除测试数据
        }
    }
}
