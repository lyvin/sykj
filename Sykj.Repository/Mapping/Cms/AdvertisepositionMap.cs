﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class AdvertisepositionMap : IEntityTypeConfiguration<Sykj.Entity.Advertiseposition>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Advertiseposition> entity)
        {
            entity.HasKey(e => e.AdvPositionId);

            entity.ToTable("ad_advertiseposition");
        }
    }
}
