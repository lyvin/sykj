﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class ContentMap : IEntityTypeConfiguration<Sykj.Entity.Content>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Content> entity)
        {
            entity.HasKey(e => e.ContentId);

            entity.ToTable("cms_content");

        }
    }
}
