﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Sykj.Repository
{
    public class PermissionsMap : IEntityTypeConfiguration<Sykj.Entity.Permissions>
    {
        public void Configure(EntityTypeBuilder<Sykj.Entity.Permissions> entity)
        {
            entity.HasKey(e => e.PermissionId);

            entity.ToTable("accounts_permissions");

        }
    }
}
