﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sykj.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRolepermissions : Sykj.Repository.IRepository<Sykj.Entity.Rolepermissions>
    {
        /// <summary>
        /// 根据角色对应所有权限
        /// </summary>
        /// <returns></returns>
        List<Sykj.Entity.Rolepermissions> GetRolePermissions();
    }
}
