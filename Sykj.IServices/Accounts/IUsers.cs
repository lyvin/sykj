﻿using Sykj.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Sykj.IServices
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUsers : Sykj.Repository.IRepository<Sykj.Entity.Users>
    {

        /// <summary>
        /// 添加用户和角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool AddRoles(Sykj.Entity.Users model);

        /// <summary>
        /// 用户名是否存在
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        bool IsExists(string userName);

        /// <summary>
        /// 通过unionId获取用户
        /// </summary>
        /// <param name="unionId"></param>
        /// <returns></returns>
        bool IsExistsUnionId(string unionId);

        /// <summary>
        /// 检测登录
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        ApiResult ValidateLogin(string userName, string passWord);

        /// <summary>
        /// 根据用户获取角色列表
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        List<Sykj.Entity.Roles> GetRoleList(int userId);

        /// <summary>
        /// 根据用户获取权限列表
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="rolesList">用户所属的角色列表</param>
        /// <returns></returns>
        List<Sykj.Entity.Permissions> GetPermissionsList(int userId, List<Sykj.Entity.Roles> rolesList);

        /// <summary>
        /// 修改用户和角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateUserRole(Sykj.Entity.Users model);

        // <summary>
        /// 管理后台查询
        /// </summary>
        /// <param name="pageIndex">当前页</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="predicate">条件</param>
        /// <param name="ordering">排序</param>
        /// <param name="args">条件参数</param>
        /// <returns></returns>
        IQueryable GetPagedListExt(int pageIndex, int pageSize, string predicate, string ordering, params object[] args);

    }
}
